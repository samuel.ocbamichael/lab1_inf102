package INF102.lab1.triplicate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MyTriplicate<T> implements ITriplicate<T> {

    @Override
    public T findTriplicate(List<T> list) {
        HashMap<T, Integer> newlist = new HashMap();
        for (int i=0; i<list.size()-1; i++) {
        	if(newlist.containsKey(list.get(i))) {
        		newlist.put(list.get(i), newlist.get(list.get(i))+1);
        	} 
        	else
        		newlist.put(list.get(i), 1);
        }
         T element = null;
        for(Map.Entry<T, Integer> entry : newlist.entrySet()) {
        	if(entry.getValue()==3) 
        		element=entry.getKey();
        		
        }
      return element;
    }
    
}
